//app.js
App({

  onLaunch: function () {
    var that = this
    //用户登陆
    wx.login({
      success: function () {
        wx.getUserInfo({
          success: function (res) {
            var simpleUser = res.userInfo;
            //console.log(simpleUser);
            that.globalData.userInfo.code = simpleUser.nickname
          }
        });
      }
    });
  
  },

  globalData:{
    //用户基本信息
    userInfo:{
      code:null,   //用户code码
      isRegister: false,   //是否为拥有项目展示资格的用户
      isTeamMenber:false,    //是否为团队成员
      use_url:"http://192.168.1.145:8087",
      real_url:"https://www.dechengdai.com"
    }
  }
 
})
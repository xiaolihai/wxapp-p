// projects.js

var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: app.globalData.userInfo.real_url,
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    //scrollView滚到顶部

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.request({
      method: 'POST',
      url:  'https://www.dechengdai.com/home/main/bnlist', //仅为示例，并非真实的接口地址
      header: {
        "Content-Type": "application/json"
      },
      success: function (res) {
        that.setData({
          bannerImg: res.data.list,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  //scrollView滚到设置位置
  tapMoveforward: function (e) {

    console.log(e);

    if (e.currentTarget.dataset.type === "cell1") {

      this.setData({
        scrollTop1: this.data.scrollTop1 + 200
      })

    }
    if (e.currentTarget.dataset.type === "cell2") {

      this.setData({
        scrollTop2: this.data.scrollTop2 + 200
      })

    }
    if (e.currentTarget.dataset.type === "cell3") {

      this.setData({
        scrollTop3: this.data.scrollTop3 + 200
      })

    }
  },

  //scrollView回滚
  scrollToTop: function (e) {

    console.log(e);

    if (e.currentTarget.dataset.type === "cell1") {

      this.setData({
        scrollTop1: 0
      })

    }
    if (e.currentTarget.dataset.type === "cell2") {

      this.setData({
        scrollTop2: 0
      })

    }
    if (e.currentTarget.dataset.type === "cell3") {
      this.setData({
        scrollTop3: 0
      })
    }
  },



  //页面跳转
  gotoDetail: function (e) {
    // console.log(e);
    let type = e.currentTarget.dataset.type;

    wx.navigateTo({
      url: '/pages/projects/projectsDetail?type=' + type
    })
  },


})
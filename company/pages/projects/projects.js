//var url = "http://192.168.1.145:8088/home/main/index_class";
var url = "https://www.dechengdai.com/home/main/index_class";
var page = 0;
var type = 11;
var limit =5;
// 请求数据
var loadMore = function (that) {
  that.setData({
    hidden: false
  });
  wx.request({
    method:"POST",
    url: url,
    data: {
      page: page,
      type: type,
      limit: limit,
    },
    success: function (res) {
      var list = that.data.list;
      console.log(res.data.list)
      for (var i = 0; i < res.data.list.length; i++) {
        
        res.data.list[i].borrow_money = (res.data.list[i].borrow_money) / 10000 
        res.data.list[i].progress = parseInt(res.data.list[i].progress)
        res.data.list[i].borrow_interest_rate = parseFloat(res.data.list[i].borrow_interest_rate).toFixed(1)
        res.data.list[i].borrow_duration = res.data.list[i].borrow_duration.substr(0, res.data.list[i].borrow_duration.indexOf("个"))
        list.push(res.data.list[i]);
      }
      that.setData({
        list: list
      });
      page++;
      that.setData({
        hidden: true
      });
    }
  });
}
Page({
  data: {
    hidden: true,
    list: [],
    scrollTop: 0,
    scrollHeight: 0
  },
  onLoad: function () {
    //   这里要注意，微信的scroll-view必须要设置高度才能监听滚动事件，所以，需要在页面的onLoad事件中给scroll-view的高度赋值
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          scrollHeight: res.windowHeight
        });
      }
    });
    loadMore(that);
  },
  //页面滑动到底部
  bindDownLoad: function () {
    var that = this;
    loadMore(that);
  },
  scroll: function (event) {
    //该方法绑定了页面滚动时的事件，我这里记录了当前的position.y的值,为了请求数据之后把页面定位到这里来。
    this.setData({
      scrollTop: event.detail.scrollTop
    });
  },
  topLoad: function (event) {
    //   该方法绑定了页面滑动到顶部的事件，然后做上拉刷新
    page = 0;
    this.setData({
      list: [],
      scrollTop: 0
    });
    loadMore(this);
  }
})